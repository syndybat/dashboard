<?php

use Illuminate\Database\Seeder;
use App\Dashboard;
use App\Tile;

class DashboardTileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('dashboards')->insert([
         'title' => 'SA',
         'description' => str_random(30),
     ]);
     DB::table('dashboards')->insert([
        'title' => 'ES',
        'description' => str_random(30),
    ]);
    DB::table('dashboards')->insert([
       'title' => 'IT',
       'description' => str_random(30),
   ]);

   DB::table('tiles')->insert([
      'title' => 'overview',
      'description' => str_random(30),
      'link' => 'http://www.seznam.cz',
  ]);

  DB::table('tiles')->insert([
     'title' => 'kpi',
     'description' => str_random(30),
     'link' => 'http://www.seznam.cz'
 ]);

 DB::table('tiles')->insert([
    'title' => 'overview',
    'description' => str_random(30),
    'link' => 'http://www.seznam.cz'
]);

$dashboard = Dashboard::find(1);
$tile = Tile::find(1);
$dashboard->tiles()->attach($tile);

    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('link');
            $table->string('description')->nullable();
            $table->string('icon_class')->nullable();
            $table->string('color_class')->nullable();
            $table->timestamps();
        });

        Schema::create('dashboard_tile', function (Blueprint $table) {
            $table->integer('dashboard_id')->unsigned();
            $table->foreign('dashboard_id')->references('id')->on('dashboards')->onDelete('cascade');

            $table->integer('tile_id')->unsigned();
            $table->foreign('tile_id')->references('id')->on('tiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard_tile');
        Schema::dropIfExists('tiles');
    }
}

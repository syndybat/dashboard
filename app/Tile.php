<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tile extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'title', 'link', 'description', 'icon_class', 'color_class',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'id',
  ];

    public function tiles(){
      return $this->belongsToMany('App\Dashboard');
    }
}

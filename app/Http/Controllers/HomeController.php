<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Tile;

class HomeController extends Controller
{
  public function index()
  {
      $tiles = Tile::all();
      return view('home', ['tiles'=>$tiles]);
  }
}

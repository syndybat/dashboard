<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dashboard;


const PAGINATION_LEVEL = 20;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dashboards = Dashboard::paginate(PAGINATION_LEVEL);
        return view('dashboards',['dashboards' => $dashboards]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.addFormDashboard');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'title' => 'required|max:255',
     ]);

     \DB::table('dashboards')->insert([
       ['title' => $request->input('title'), 'description' => $request->input('description'), 'public'=>$request->input('public')]
     ]);

     return redirect('admin/dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dashboard = Dashboard::findOrFail($id);
        return view('dashboard.showDashboard', ['dashboard'=> $dashboard]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dashboard = Dashboard::findOrFail($id);
        return view('dashboard.addFormDashboard', ['dashboard'=>$dashboard]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'title' => 'required|max:255',
     ]);

        $dashboard = Dashboard::findOrFail($id);
        $dashboard->title = $request->input('title');
        $dashboard->description = $request->input('description');
        $dashboard->public = $request->input('public');
        $dashboard->save();

        return redirect('admin/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dashboard = Dashboard::findOrFail($id);
        $dashboard->delete();

        return redirect('admin/dashboard');
    }
}

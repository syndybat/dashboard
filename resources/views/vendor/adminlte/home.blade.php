@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">

				<div class="col-lg-3 col-sm-6">
                <div class="small-box bg-yellow tile">
                    <div class="inner">
                      <a href="http://truv244x.tu.cz.conti.de/dashboard-ES/www/">
                        <h3 class="relative">ES</h3>
                        <p>Trutnov</p>
                        <div class="icon">
                            <i class="ion-document-text"></i>
                          </div>
                        </a>
                    </div>
                    <a href="http://truv244x.tu.cz.conti.de/dashboard-ES/www/" class="small-box-footer">Zobrazit <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

						<div class="col-lg-3 col-xs-6">
						          <!-- small box -->
						          <div class="small-box bg-red">
						            <div class="inner">
						              <h3>65</h3>

						              <p>Unique Visitors</p>
						            </div>
						            <div class="icon">
						              <i class="ion ion-pie-graph"></i>
						            </div>
						            <a href="#" class="small-box-footer">
						              More info <i class="fa fa-arrow-circle-right"></i>
						            </a>
						          </div>
						        </div>
		
@endsection

@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
	{{ trans('ui.dashboard') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">

				<div class="col-lg-12">
		<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{ trans('ui.dashboards') }}</h3>
							<div class="pull-right clearfix" >
								<a class='btn btn-default' href={{route('dashboard.create')}}>{{trans('ui.addNewDashboard')}}</a>
							</div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">

                <tbody>
									<tr>
	                  <th style="width: 10px">#</th>
	                  <th>{{ trans('ui.title') }}</th>
	                  <th>{{ trans('ui.description') }}</th>
										<th style="width: 40px">{{ trans('ui.show') }}</th>
										<th style="width: 40px">{{ trans('ui.edit') }}</th>
										<th style="width: 40px">{{ trans('ui.delete') }}</th>
	                </tr>
									@foreach ($dashboards as $dashboard)
										<tr>
		                  <td>{{$dashboard->id}}</td>
		                  <td>{{$dashboard->title}}</td>
		                  <td>{{$dashboard->description}}</td>
											<td><a class="btn btn-small btn-success" href="{{ route('dashboard.show', ['id' => $dashboard->id]) }}">{{ trans('ui.show') }}</a></td>
											<td><a class="btn btn-small btn-success" href="{{  route('dashboard.edit', ['id' => $dashboard->id]) }}">{{ trans('ui.edit') }}</a></td>
											<td>
												<form method="POST" action="{{ route('dashboard.destroy', ['id' => $dashboard->id])}}">
													{!! csrf_field() !!}
													<input type="hidden" name="_method" value="DELETE">
													<button class="btn btn-small btn-danger">{{ trans('ui.delete') }} </button>
												</form>
											</td>
										</tr>
									@endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                {{$dashboards->links()}}
              </ul>
            </div>
          </div>
				</div>
			</div>
		</div>

@endsection

@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
		<div class="row">
      @foreach ($tiles as $tile)
        <div class="col-lg-3 col-sm-6">
                <div class="small-box {{$tile->color_class or 'bg-yellow'}} tile">
                    <div class="inner">
                      <a href="{{$tile->link}}">
                        <h3 class="relative">{{$tile->title}}</h3>
                        <p>{{$tile->description}}</p>
												@if($tile->icon_class)
                        <div class="icon">
                            <i class="{{$tile->icon_class}}"></i>
                          </div>
												@endif
                        </a>
                    </div>
                    <a href="{{$tile->link}}" class="small-box-footer">Zobrazit <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
      @endforeach

 </div>
@endsection

@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
	{{ trans('ui.dashboard') }}
@endsection


@section('main-content')
  <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">{{trans('ui.dashboardForm')}}</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form class="form-horizontal" method="POST" action={{ isset($dashboard) ? route('dashboard.update',['id'=>$dashboard->id]) : route('dashboard.store')  }}>
                <div class="box-body">
                  @isset($dashboard->id)
                    {{method_field('PATCH') }}
                    <input id="id" name="id" type="hidden" value={{$dashboard->id}}>
                  @endisset
                  {{ csrf_field() }}

                  <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">{{trans('ui.title')}}</label>

                    <div class="col-sm-10">
                      <input class="form-control" label={{trans('ui.title')}} type="text" name="title" value="{{ $dashboard->title or old('title') }}"></input>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">{{trans('ui.description')}}</label>

                    <div class="col-sm-10">
                      <textarea style="resize:none" name="description" class="form-control" rows="3" placeholder="" label={{trans('ui.description')}}>{{ $dashboard->description or old('description') }}</textarea>
                    </div>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">{{trans('ui.submit') }}</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
@endsection

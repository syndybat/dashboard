<?php

return [


    /*
    |--------------------------------------------------------------------------
    | User interface language
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
  'dashboard' => 'Dashboard',
  'dashboards' => 'Dashboards',
  'title' => 'Title',
  'description' => 'Description',
  'public' => 'Public',
  'show' => 'Show',
  'edit' => 'Edit',
  'delete' => 'Delete',
  'submit' => 'submit',

  /*
  |--------------------------------------------------------------------------
  | add dasboard form
  |--------------------------------------------------------------------------
  |
  |
  */

  'dashboardForm' => 'Dashboard form',
  'yes' => 'Yes',
  'no' => 'No',
  'addNewDashboard'=>'Add new dashboard',

  /*
  |--------------------------------------------------------------------------
  | showdashboard
  |--------------------------------------------------------------------------
  |
  |
  */
  'showDashboard'=>'ShowDashboard',
  'assignedTiles' => 'Assigned Tiles',




];
